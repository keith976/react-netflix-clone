import React, { useState } from 'react';
import axios from 'axios';

const movieSearchDatabaseURL = "https://api.themoviedb.org/3/search/movie?language=en-US&page=1&include_adult=false"
const api_key = "c6e8c80adc86ea899f6d247e902ffc54";

const MovieSearch = () => {
  const [query, setQuery] = useState();
  const handleInput = (e) =>{
    const { value } = e.target; 
    setQuery(value);
  }
  const handleSearch = () =>{
    axios.get(movieSearchDatabaseURL + '&query=' + query + '&api_key=' + api_key)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <>
      <h3>Movie Name:</h3>
      <input name="query" onChange={handleInput}></input>
      <button onClick={handleSearch}>Search</button>
    </>
  );
}
 
export default MovieSearch;
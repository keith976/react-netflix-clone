import React from 'react';
import axios from 'axios';
import './App.scss';
import MovieSearch from './MovieSearch/MovieSearch';
import MovieCard from './MovieCard/MovieCard';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const movieTrendingDatabaseURL = 'https://api.themoviedb.org/3/trending/all/week';
const movieSearchDatabaseURL = "https://api.themoviedb.org/3/search/movie?language=en-US&page=1&include_adult=false"
const api_key = 'c6e8c80adc86ea899f6d247e902ffc54';

class App extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      movies: [],
      query: '',
    };
  }
  componentDidMount(){
    axios.get(movieTrendingDatabaseURL + '?api_key=' + api_key)
      .then((response) => {
        this.setState({movies: response.data.results});
      })
      .catch((error) => {
        console.log(error);
      });
  }
  handleInput = (e) =>{
    const { value } = e.target;
    this.setState({ query: value });
    console.log(this.state.query);
  }
  handleSearch = () =>{
    axios.get(movieSearchDatabaseURL + '&query=' + this.state.query + '&api_key=' + api_key)
      .then((response) => {
        if (response.data.results.length > 0){
          this.setState({movies: response.data.results});
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render(){
    return (
      <Container className="main">
        <Row className="header__row">
          <Col>
            <h1>React Movie Search</h1>
          </Col>
        </Row>
        <Row className="movie__search__row">
          <Col className="content">
            <h3>Movie Name:</h3>
            <input name="query" onChange={this.handleInput}></input>
            <button onClick={this.handleSearch}>Search</button>
          </Col>
        </Row>
        <Row className="movie__card__row">
          {
            (this.state.movies && this.state.movies.length > 0) && 
            this.state.movies.filter(movie => movie.poster_path && movie.backdrop_path).map((movie) => 
              <MovieCard 
                title={movie.title ? movie.title : movie.name}
                overview={movie.overview}
                poster={movie.poster_path}
                backdrop={movie.backdrop_path}
                key={movie.id}
              />
            )
          }
        </Row>
      </Container>
    );
  }
}

export default App;

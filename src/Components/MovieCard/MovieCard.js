import React from 'react';

import Col from 'react-bootstrap/Col';

const imageUrlPrefix = "https://image.tmdb.org/t/p/original/";

const MovieCard = (props) => {
  return (
    <Col className="card" lg={2} md={3} sm={6}>
      <img src={imageUrlPrefix + props.backdrop}/>
      <div className="card__popup">
        <img src={imageUrlPrefix + props.poster}/>
        <div className="information__container">
          <span>{props.title}</span>
          <span>{props.overview}</span>
        </div>
      </div>
    </Col>
  );
}

export default MovieCard;